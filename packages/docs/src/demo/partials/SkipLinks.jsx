const React = require('react');

const MetalsmithContext = require('../../../build/MetalsmithContext');

module.exports = () => {
    const { loggedIn, translate } = React.useContext(MetalsmithContext);

    return (
        <>
            {!loggedIn && (
                <a class="aui-skip-link" href="/demo/pages/login.html">
                    {translate('Log in')}
                </a>
            )}
            <a class="aui-skip-link" href="#main">
                {translate('Skip to main content')}
            </a>
        </>
    );
};
