const React = require('react');

const MetalsmithContext = require('../../../build/MetalsmithContext');

const Button = require('../components/Button');

const PageMoreDropdown = require('./PageMoreDropdown');

module.exports = () => {
    const { loggedIn, page, translate } = React.useContext(MetalsmithContext);

    const hideActions = !loggedIn;

    // TODO breadcrumb navigation does not match the tree

    return (
        <div class="aui-page-header-inner">
            <div class="aui-page-header-image">
                <span class="aui-avatar aui-avatar-large aui-avatar-project">
                    <span class="aui-avatar-inner">
                        <img
                            alt="My awesome project"
                            src="https://api.adorable.io/avatars/200/abott2@adorable.png"
                        />
                    </span>
                </span>
            </div>
            <div class="aui-page-header-main">
                {/* TODO: Re-enable breadcrumbs */}
                {/* <ol class="aui-nav aui-nav-breadcrumbs">
                    <li>
                        <a href="#">Breadcrumbs</a>
                    </li>
                    <li>
                        <a href="#" class="custom">
                            Sub-page
                        </a>
                    </li>
                    <li class="aui-nav-selected">
                        Direct parent page{' '}
                        <span class="aui-icon aui-icon-small aui-iconfont-unlock-filled">
                            {translate('unrestricted')}
                        </span>
                    </li>
                </ol> */}
                <h1>{page.label}</h1>
            </div>
            {hideActions === false && (
                <>
                    <div class="aui-page-header-actions">
                        <div class="aui-buttons">
                            <Button variant="subtle">
                                <span class="aui-icon aui-icon-small aui-iconfont-new-edit" />{' '}
                                {translate('Edit')}
                            </Button>
                            <Button variant="subtle">
                                <span class="aui-icon aui-icon-small aui-iconfont-new-star" />{' '}
                                {translate('Save for later')}
                            </Button>
                            <Button variant="subtle">
                                <span class="aui-icon aui-icon-small aui-iconfont-watch-filled" />{' '}
                                {translate('Watch')}
                            </Button>

                            <Button
                                variant="subtle"
                                controlledInlineDialogId="share-page-inline-dialog"
                            >
                                <span class="aui-icon aui-icon-small aui-iconfont-share" />{' '}
                                {translate('Share')}
                            </Button>

                            <aui-inline-dialog
                                id="share-page-inline-dialog"
                                alignment="bottom center"
                                aria-label={translate('Share page')}
                            >
                                <h3 id="share-page-form--header">
                                    {translate('Share page')}
                                </h3>
                                <form
                                    class="aui top-label"
                                    id="share-page-form"
                                >
                                    <div class="field-group">
                                        <label for="share-page--email">
                                            {translate('Email address')}
                                            <span class="aui-icon icon-required">
                                                ({translate('required')})
                                            </span>
                                        </label>
                                        <input
                                            required
                                            class="text medium-field"
                                            type="email"
                                            id="share-page--email"
                                            name="email"
                                        />
                                    </div>

                                    <div class="buttons-container">
                                        <div class="buttons">
                                            <Button
                                                variant="primary"
                                                type="submit"
                                            >
                                                {translate('Share')}
                                            </Button>
                                        </div>
                                    </div>
                                </form>
                            </aui-inline-dialog>

                            {/* <PageMoreDropdown /> */}
                        </div>
                    </div>
                </>
            )}
        </div>
    );
};
