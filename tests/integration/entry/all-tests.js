// Setup
import '../setup';
import '../bootstrap';
// Get all of AUI (mainly for the CSS)
import '@atlassian/aui/entry/aui.batch.prototyping';
// Run the tests
import '../unit';
