import './helpers/mock-require';
import { afterMutations, removeLayers } from './helpers/all';
import { DELAY } from '@atlassian/aui/src/js/aui/internal/elements';
import $ from 'jquery';

let fixtureElement;

/**
 * Sets the el's innerHTML to '' and executes the next callback after any
 * DOM mutation handlers (e.g., skate's detached callbacks) have had a
 * chance to run.
 */
function clearContents(el, next) {
    afterMutations(() => {
        el.innerHTML = '';
        next && afterMutations(next);
    });
}

before(() => {
    window.onbeforeunload = () => console.trace('Uh oh! Tried unloading the page :(');
});

beforeEach(function (done) {
    fixtureElement = document.getElementById('test-fixture');
    if (!fixtureElement) {
        fixtureElement = document.createElement('div');
        fixtureElement.id = 'test-fixture';
        document.body.appendChild(fixtureElement);
    }

    // reset the URL hash so tests can make assertions based on it
    window.location.hash = '';
    // need to wait a bit to avoid capturing an initial hashchange event
    afterMutations(done);
});

afterEach(done => {
    if (setTimeout.clock) {
        setTimeout.clock.restore();
    }

    // unbind some test-specific handlers that might've not been cleaned up
    $(window).off('.aui-test-suite');
    $(document).off('.aui-test-suite');

    clearContents(fixtureElement, () => {
        $('body, html').css('overflow', '');
        removeLayers();
        afterMutations(done, DELAY + 1);
    });
});
