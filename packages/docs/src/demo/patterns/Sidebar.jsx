const React = require('react');
const classnames = require('classnames');

const makeHref = require('../helpers/makeHref');

const MetalsmithContext = require('../../../build/MetalsmithContext');

const isParentOfCurrent = (item, loggedIn, pageUrl, lang) => {
    return item.children.some((child) => {
        const href = makeHref(child.id, loggedIn, lang);
        return href === pageUrl || isParentOfCurrent(child, loggedIn, pageUrl, lang);
    });
};

const Nav = ({ items }) => {
    const { loggedIn, pageUrl, lang } = React.useContext(MetalsmithContext);
    if (items.length === 0) {
        return null;
    }

    return (
        <ul class="aui-nav">
            {items.map((item, index) => {
                const isParent = item.children.length > 0;
                const isExpanded = isParentOfCurrent(item, loggedIn, pageUrl, lang);

                const className = classnames({
                    'aui-icon': true,
                    'aui-icon-small': true,
                    'aui-iconfont-collapsed': !isExpanded,
                    'aui-iconfont-expanded': isExpanded,
                });

                const href = makeHref(item.id, loggedIn, lang);
                const ariaCurrent = href === pageUrl ? 'page' : undefined;

                return isParent ? (
                    <li key={index} aria-expanded={isExpanded.toString()}>
                        <button class="aui-nav-subtree-toggle">
                            <span class={className}>
                                {isExpanded ? 'Collapse' : 'Expand'}
                            </span>
                        </button>
                        <a
                            class="aui-nav-item"
                            href={href}
                            aria-current={ariaCurrent}
                        >
                            <span class="aui-nav-item-label">{item.label}</span>
                        </a>
                        <Nav items={item.children} />
                    </li>
                ) : (
                    <li key={index}>
                        <a
                            class="aui-nav-item nested-parent nested-open"
                            href={href}
                            aria-current={ariaCurrent}
                        >
                            <span class="aui-nav-item-label">{item.label}</span>
                        </a>
                    </li>
                );
            })}
        </ul>
    );
};

const spaceNavData = [
    ['page', 'Pages', true],
    ['quote', 'Blog'],
    ['question-circle', 'Questions'],
    ['calendar', 'Calendars'],
];

module.exports = () => {
    const { sidebarItems, space, page, translate } = React.useContext(MetalsmithContext);

    return (
        <section class="aui-sidebar" aria-label="Space sidebar">
            <div class="aui-sidebar-wrapper">
                <div class="aui-sidebar-body">
                    <header class="aui-page-header">
                        <div class="aui-page-header-inner">
                            <div class="aui-page-header-image">
                                <span class="aui-avatar aui-avatar-project aui-avatar-large">
                                    <span class="aui-avatar-inner">
                                        <img
                                            src={`https://api.adorable.io/avatars/200/${page.spaceId}@adorable.png`}
                                            alt=""
                                            role="presentation"
                                        />
                                    </span>
                                </span>
                            </div>
                            <div class="aui-page-header-main">
                                <h1>{space}</h1>
                            </div>
                        </div>
                    </header>

                    <nav
                        class="aui-navgroup aui-navgroup-vertical"
                        aria-label="Space"
                    >
                        <div class="aui-navgroup-inner">
                            <div class="aui-sidebar-group aui-sidebar-group-tier-one">
                                <div class="aui-nav-heading" title="Space">
                                    <strong>{translate('Space')}</strong>
                                </div>
                                <ul class="aui-nav">
                                    {spaceNavData.map(
                                        ([id, label, isSelected], index) => (
                                            <li
                                                key={index}
                                                className={
                                                    isSelected
                                                        ? 'aui-nav-selected'
                                                        : undefined
                                                }
                                            >
                                                <a
                                                    class="aui-nav-item"
                                                    href="#"
                                                >
                                                    <span
                                                        class={
                                                            'aui-icon aui-icon-small aui-iconfont-' +
                                                            id
                                                        }
                                                    ></span>

                                                    <span class="aui-nav-item-label">
                                                        {translate(label)}
                                                    </span>
                                                </a>
                                            </li>
                                        )
                                    )}
                                </ul>
                            </div>

                            <div class="aui-sidebar-group aui-sidebar-group-actions">
                                <div class="aui-nav-heading" title="Pages">
                                    <strong>{translate('Pages')}</strong>
                                </div>

                                <Nav items={sidebarItems} />
                            </div>
                        </div>
                    </nav>
                </div>

                <div class="aui-sidebar-footer">
                    <a
                        href="https://example.com"
                        class="aui-button aui-button-subtle aui-sidebar-settings-button"
                        title="Settings"
                    >
                        <span class="aui-icon aui-icon-small aui-iconfont-configure"></span>
                        <span class="aui-button-label">{translate('Settings')}</span>
                    </a>
                    <button
                        class="aui-button aui-button-subtle aui-sidebar-toggle aui-sidebar-footer-tipsy"
                        title="Collapse sidebar ( [ )"
                    >
                        <span class="aui-icon aui-icon-small aui-iconfont-chevron-double-left"></span>
                    </button>
                </div>
            </div>
        </section>
    );
};
