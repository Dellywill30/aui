/* eslint-disable */

(function () {
    // Shows the dialog when the "Show dialog" button is clicked
    $('#create-page--show-button').on('click', function (e) {
        e.preventDefault();
        AJS.dialog2('#create-page--dialog').show();
    });

    // Hides the dialog
    $('#create-page--cancel-button').on('click', function (e) {
        e.preventDefault();
        AJS.dialog2('#create-page--dialog').hide();
    });
    // Submits the form and hides the dialog
    $('#create-page-form').on('submit', function (e) {
        const lang = document.documentElement.lang;
        const messages = {
            pl: ({ title, space }) =>
                `Strona <strong>${AJS.escapeHtml(
                    title
                )}</strong> została utworzona w przestrzeni <strong>${AJS.escapeHtml(
                    space
                )}</strong>.`,
            en: ({ title, space }) =>
                `The page <strong>${AJS.escapeHtml(
                    title
                )}</strong> has been created in space <strong>${AJS.escapeHtml(
                    space
                )}</strong>.`,
        };

        e.preventDefault();
        const space = $('#create-page--space').val();
        const title = $('#create-page--title').val();
        AJS.dialog2('#create-page--dialog').hide();
        AJS.flag({
            type: 'success',
            body: messages[lang]({ title, space }),
        });
    });
})();

(function () {
    AJS.$(function () {
        var inlineDialog = document.getElementById('share-page-inline-dialog');

        $('#share-page-form').on('submit', function (e) {
            const lang = document.documentElement.lang;
            const messages = {
                pl: ({ email }) =>
                    `Ta strona została udostępniona dla <strong>${AJS.escapeHtml(
                        email
                    )}</strong>.`,
                en: ({ email }) =>
                    `This page has been shared with <strong>${AJS.escapeHtml(
                        email
                    )}</strong>.`,
            };

            e.preventDefault();
            const email = $('#share-page--email').val();
            setTimeout(() => {
                AJS.flag({
                    type: 'success',
                    body: messages[lang]({ email }),
                });
            }, 1000);
            inlineDialog.open = false;
        });
    });
})();

(function () {
    function checkAuthStatus() {
        const user = localStorage.getItem('user');

        if (user === null) {
            const newHref = window.location.href
                .split('/')
                .slice(0, -1)
                .concat('viewpage.action.logged-out.html')
                .join('/');
            return (window.location.href = newHref);
        }
    }

    window.addEventListener('load', checkAuthStatus);

    $('#log-out').on('click', function (e) {
        localStorage.removeItem('user');

        window.location.href = `/demo/${document.documentElement.lang}/pages/login.html`;

        // We no longer rediect to logged out version of page.
        // Above you can see redirecto to the login form instead.
        //
        // checkAuthStatus();
    });
})();
