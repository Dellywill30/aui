const React = require('react');

const MetalsmithContext = require('../../../build/MetalsmithContext');

module.exports = ({ children }) => {
    const { translate } = React.useContext(MetalsmithContext);

    return (
        <>
            <a
                href="#dropdown2-avatar"
                aria-owns="dropdown2-avatar"
                aria-haspopup="true"
                class="aui-dropdown2-trigger-arrowless aui-dropdown2-trigger"
                aria-controls="dropdown2-avatar"
                aria-expanded="false"
            >
                {children}
            </a>

            <aui-dropdown-menu id="dropdown2-avatar">
                <aui-section label="Favourites">
                    <aui-item-link href="#">Amazon</aui-item-link>
                    <aui-item-link href="#">Apple</aui-item-link>
                    <aui-item-link href="#">Facebook</aui-item-link>
                    <aui-item-link href="#">Google</aui-item-link>
                </aui-section>
                <aui-section label="Recently visited">
                    <aui-item-link href="#">Atlassian</aui-item-link>
                </aui-section>
                <aui-section>
                    <aui-item-link href="#" id="log-out">
                        {translate('Log out')}
                    </aui-item-link>
                </aui-section>
            </aui-dropdown-menu>
        </>
    );
};
