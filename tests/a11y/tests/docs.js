const { run } = require('../runner');

const spaces = ['personal-space', 'space-it-support'];

const baseUrl = 'http://localhost:8000/';
const unauthenticatedPaths = ['demo/en', 'demo/en/pages/login.html'].concat(
    spaces.map((space) => `demo/en/pages/${space}/viewpage.action.logged-out.html`)
);

run({ name: 'docs', baseUrl, paths: unauthenticatedPaths });

const authenticatedPaths = spaces.map(
    (space) => `demo/en/pages/${space}/viewpage.action.html`
);

run(
    { name: 'docs', baseUrl, paths: authenticatedPaths },
    {
        actions: [
            `navigate to ${baseUrl}/demo/en/pages/login.html`,
            'click element #log-in-submit-button',
            'wait for path to not be /demo/en/pages/login.html',
        ],
    }
);
