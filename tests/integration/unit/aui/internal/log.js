import * as log from '@atlassian/aui/src/js/aui/internal/log';

describe('aui/internal/log', function () {
    it('globals', function () {
        expect(AJS.log.toString()).to.equal(log.log.toString());
        expect(AJS.error.toString()).to.equal(log.error.toString());
        expect(AJS.warn.toString()).to.equal(log.warn.toString());
    });
});
