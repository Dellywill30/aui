const React = require('react');
const classnames = require('classnames');

module.exports = ({
    children,
    id,
    form,
    variant,
    type,
    controlledInlineDialogId,
    controlledDropdownId,
}) => {
    const className = classnames({
        'aui-button': true,
        'aui-button-subtle': variant === 'subtle',
        'aui-button-primary': variant === 'primary',
        'aui-dropdown2-trigger aui-dropdown2-trigger-arrowless': controlledDropdownId,
    });

    const extraProps = {
        ...(!controlledInlineDialogId
            ? {}
            : {
                  'data-aui-trigger': 'true',
                  'aria-controls': controlledInlineDialogId,
              }),
        ...(!controlledDropdownId
            ? {}
            : {
                  'aria-haspopup': 'true',
                  'aria-expanded': 'false',
                  'aria-controls': controlledDropdownId,
              }),
    };

    return (
        <button id={id} form={form} class={className} type={type} {...extraProps}>
            {children}
        </button>
    );
};
