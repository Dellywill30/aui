/* eslint-disable */

const labelToId = require('../helpers/labelToId');
const textToTree = require('../helpers/textToTree');
const marked = require('marked');

const hierarchy = `
Space IT Support
    Knowledge central
        Troubleshooting articles
            Broken keyboard
            Unable to access VPN
            Resolving Connection Issues on Your WIFI Nework
            Troubleshoot PDF Printing
        How-to articles
            Installing Microsoft Excel
            Macbook Pro Setup for new users
            Mac OS X: How to add a network printer
            Configuring the Mac email client
            New MacBook Pro request - process and system specs
            Storage array upgrade change guidelines
            VPN client update for Mac
            Windows 10 Mail and Calendar setup
            MS Office 365 for business - setup
            Visiting an Office - What you need to know and do
            New Lenovo Thinkpad Setup (Notebook)
            How to set up your Google Calendar
            How to Connect to the WiFi Network from a Mac
            How to connect to the printers in the office
            How to Connect to the WiFi Network from a PC
            Employee Mobile Policy
    Service catalog
        Email and collaboration tools
        Server hosting
        Web conferencing
    Major incident response - troubleshooting guides 
    Site reliability engineering
Kosmos Operations
    Meeting notes
        2020-02-20 Meeting notes
        Meeting notes template
        Kosmos Conference 2020
    Product planning
        Kosmos 2.0 - Stakeholder update
        Kosmos Finance Report
        Kosmos Pricing Guidelines
        Kosmos Roadmap
    Product Requirements
        Launch Pad Product Requirements
        Mobile Web Requirements
    Kosmos PR
        Summer Saturn Sizzle Blog Announcement
        Kosmos Marketing Launch
Personal space
    Personal growth
        Performance reviews
        Goals 2020
        Goals 2019
        Goals 2018
    Talks and presentations
        Space Conference 2020
        Talk submissions
Welcome
`;

const spaceItSupportOverview = `
## Welcome to Kosmos IT Support. How can we help?

### Our services include

- Setting up your new hardware and software.
- Making sure everything is secure in the company
- Cloud platform maintenance.
- Essential system configuration
- Inspection and repairs of laptops and other personal devices.
- Server maintenance and repairs.

### If I have a problem

- First, check out the list of troubleshooting pages in the navigation.
- If there’s no article corresponding to your issue, contact us directly through the Service Portal.
`;

const welcomeOverview = `
## News

### Our new podcast - The final frontier

Published today

Yesterday we launched our first podcast - The final frontier. 
We hope it’s going to be the next hip place to talk all the things kosmos, physics and life. 
It’s also a place where we want to share “story behind the story” of Kosmos teams that have done amazing things together. Please don’t forget to subscribe on Spotify and Apple Podcasts. 

### Big Kosmos Party - annoucement

Published 4 days ago

It’s almost here - our big annual Kosmos Party. 
And this year it’s a special one! Join us on {today + 10 days} for some drinks, food and dancing in Sky Restaurant. 
We have lots to celebrate. It’s our 20th year, and we’re about to launch the new revolutionary product! 
`;

const brokenKeyboardContent = `
We get you, nothing is more frustrating when trying to get your work done than a broken device. 
Here's a list of simple troubleshooting advice for a keyboard that seems to be broken. 
Try these first before you run out to get a new one.

## Case 1 - I spilled coffee / water / pepsi / wine over it

Spilling a drink on your keyboard doesn’t have to be a kiss of death and a ticket to 
a replacement — if you know what to do and act quickly. 
Take a dry cloth and wipe up any excess liquid from the surface. 
Turn it upside down, put it over a towel or something absorbent, and let the water drain out of it. 
Leave it for 24hours. If you don’t have the time, th minimum time is four hours. 
Even though it seems dry, these parts do absorb a lot of water, so that just gives it time to dissipate any liquid.

## Case 2 - One key is broken

If one of the specific keys are broken, how you replace it will depend on the type of keyboard you have. 
For example, a mechanical keyboard is designed differently than a quiet-key device. 
If the key is stuck, you can use some compressed air 

Please contact our support to investigate us to order a new spare key or a new keyboard

You can go to Instructables.com for a helpful video on fixing an unresponsive key on a standard and commonly found Microsoft keyboard, using just an ordinary plastic straw.

## Case 3 - The whole keyboard is not working

Please follow this short checklist before contacting our IT support.

- Check the batteries. This sounds simple, but it's always the best place to start. Replace the batteries if you have a wireless keyboard.
- Check the connection. If you have a wired keyboard, ensure the cable has not come loose from the USB port. If you have a USB receiver for a wireless keyboard, ensure this is plugged correctly.
- Re-pair the keyboard if you're using Bluetooth technology. Although most companies promise one-time pairing, a redo is occasionally needed. Follow these step-by-step instructions on pairing Bluetooth devices.
`;

const workInProgress = `
It's work in progress.
`;

const tree = textToTree(hierarchy);

function replacer(key, value) {
    // Filtering out recursive properties
    if (key === 'parent') {
        return undefined;
    }
    return value;
}

function log(object) {
    console.log(JSON.stringify(object, replacer, 2));
}

// log(tree);

const spaces = tree.map((el) => ({ label: el.label, id: el.id }));

// log(spaces);

const flatTree = (tree, spaceId, spaceLabel) => {
    return tree.map((el) =>
        [{ id: el.id, label: el.label, spaceId, spaceLabel }].concat(
            flatTree(el.children, spaceId, spaceLabel)
        )
    ).reduce((result, next) => result.concat(next), [])
};

const pagesFromTree = tree.map((node) =>
    [
        {
            id: node.id,
            label: node.label,
            spaceId: node.id,
            spaceLabel: node.label,
        },
    ].concat(flatTree(node.children, node.id, node.label))
).reduce((result, next) => result.concat(next), [])

// log(pagesFromTree);

const pagesWithContent = {
    'broken-keyboard': {
        content: marked(brokenKeyboardContent),
    },
    'space-it-support': {
        content: marked(spaceItSupportOverview),
    },
    welcome: {
        content: marked(welcomeOverview),
    },
};

const defaultContent = marked('Work in progress.')

const labelToPage = (label) => ({ id: labelToId(label), label });

const result = {
    tree,
    pages: pagesFromTree.map((page) => {
        const pageWithContent = pagesWithContent[page.id] || {
            content: defaultContent
        };
        return Object.assign({}, page, pageWithContent);
    }),
    menus: {
        spaces,
        recent: {
            recentWork: [
                'Kosmos Conference 2020',
                '2020-02-20 Meeting notes',
                'Spaceship Marketing Lunch',
                'Configuring the Mac email client',
            ].map(labelToPage),
            recentlyVisited: [
                'Broken Keyboard',
                'Email and collaboration tools',
                'Meeting notes template',
            ].map(labelToPage),
        },
    },
};

module.exports = result;
