const React = require('react');

const Document = require('../partials/Document');
const PageHeaderInner = require('../partials/PageHeaderInner');
const PageContent = require('../partials/PageContent');
const PageFooter = require('../partials/PageFooter');

const AppHeader = require('./AppHeader');
const Sidebar = require('./Sidebar');

const MetalsmithContext = require('../../../build/MetalsmithContext');


module.exports = () => {
    const { loggedIn, page } = React.useContext(MetalsmithContext);

    return (
        <Document
            title={page.label + ' / ' + page.spaceLabel}
            scripts={[
                loggedIn
                    ? '/demo/assets/viewpage.action.js'
                    : '/demo/assets/viewpage.action.logged-out.js',
            ]}
        >
            <div id="page">
                <AppHeader />

                <div id="content">
                    <Sidebar />
                    <main id="main" class="aui-page-panel">
                        <div class="aui-page-panel-inner">
                            <section class="aui-page-panel-content">
                                <header class="aui-page-header">
                                    <PageHeaderInner />
                                </header>
                                <article class="aui-flatpack-example">
                                    <PageContent />
                                </article>
                            </section>
                        </div>
                    </main>
                </div>

                <PageFooter />
            </div>
        </Document>
    );
};
