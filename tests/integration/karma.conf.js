const webpackParts = require('@atlassian/aui-webpack-config/webpack.parts');
const merge = require('webpack-merge');
const path = require('path');

module.exports = function (config) {
    const {
        BITBUCKET_BUILD_NUMBER,
        BITBUCKET_REPO_SLUG,
        BROWSERSTACK_USER,
        BROWSERSTACK_USERNAME,
        BROWSERSTACK_ACCESS_KEY,
        BROWSERSTACK_ACCESSKEY,
        BROWSERSTACK_KEY,
        CI_BUILD_NUMBER,
    } = process.env;

    const reporters = ['progress', 'junit', 'BrowserStack'];
    config.watch || reporters.push('coverage-istanbul');

    const jsTranspileStrategy = config.watch ? webpackParts.transpileJs : webpackParts.transpileJsWithCodeCoverage;

    const webpackConfig = merge([
        webpackParts.setAuiVersion(),
        webpackParts.loadSoy(),
        webpackParts.resolveSoyDeps(),
        webpackParts.inlineCss(),
        jsTranspileStrategy({
            exclude: /(tether|soy-template-plugin-js|node_modules|mocks)/,
        }),
        {
            mode: 'development',
            devtool: false,
            externals: {
                'jquery': 'jQuery'
            },
            module: {
                rules: [
                    /**
                     *  Allows for mocking dependencies in any JS.
                     **/
                    {
                        test: /\.js$/i,
                        exclude: /(tether|soy-template-plugin-js|node_modules|mocks)/,
                        resolve: {
                            /**
                             * We use aliases to mock some low-level module dependencies in AUI.
                             * As at 2021-03-01, AUI didn't have need for a general-purpose module mocking or dependency-injection solution.
                             */
                            alias: {
                                /**
                                 *  As in our tests:
                                 *  - we are testing the Alignment (our abstraction layer),
                                 *  - not the technical / practical implementation that sits inside,
                                 *  - do not explicitly contract any async behaviour
                                 *  - want to run tests fast && sync
                                 *  we will mock internal dependency (Popper) in a way that allows for sync and async
                                 *  behaviour on demand, easily controllable in tests.
                                 *
                                 *  We use @popperjs/core$ (with $ sign at the end) to enforce exact match
                                 */
                                '@popperjs/core$': path.resolve(__dirname, 'mocks/popper.js'),
                                /**
                                 * Some tests want to check if anything got logged, so we mock our logger proxy module
                                 */
                                './log': path.resolve(__dirname, 'mocks/logger.js'),
                                './internal/log': path.resolve(__dirname, 'mocks/logger.js'),
                                /**
                                 * Some tests expect different capabilities to exist in some browsers
                                 */
                                './browser': path.resolve(__dirname, 'mocks/browser.js'),
                                './internal/browser': path.resolve(__dirname, 'mocks/browser.js'),
                            }
                        }
                    },
                    {
                        test: /\.(png|jpg|gif|svg|ttf|eot|woff|woff2)$/,
                        use: 'ignore-loader'
                    }
                ]
            }
        }
    ]);

    const testPattern = './entry/all-tests.js';
    const bootstrapPattern = './*.js';

    config.set({
        browserStack: {
            username: BROWSERSTACK_USERNAME || BROWSERSTACK_USER,
            accessKey: BROWSERSTACK_ACCESS_KEY || BROWSERSTACK_ACCESSKEY || BROWSERSTACK_KEY,
            project: BITBUCKET_REPO_SLUG || 'aui',
            build: BITBUCKET_BUILD_NUMBER || CI_BUILD_NUMBER || Date.now(),
            startTunnel: true,
            forceLocal: true,
        },
        hostname: '127.0.0.1',
        autoWatch: config.watch,
        singleRun: !config.watch,
        frameworks: ['mocha', 'sinon-chai', 'webpack'],
        browsers: config.watch ? ['Chrome_dev'] : ['Chrome_1024x768','Firefox_1024x768'],
        browserDisconnectTolerance: 3,
        browserDisconnectTimeout: 10000,
        client: {
            useIframe: true,
            mocha: {
                timeout: 12345,
                slow: 1,
                ui: 'bdd',
            },
            chai: {
                includeStack: true,
            },
        },
        customLaunchers: {
            Chrome_dev: {
                base: 'Chrome',
                flags: ['--window-size=1624,768', '--auto-open-devtools-for-tabs']
            },
            Chrome_1024x768: {
                base: 'Chrome',
                flags: ['--window-size=1024,768']
            },
            Chrome_headless: {
                base: 'ChromeHeadless',
                flags: ['--disable-translate', '--disable-extensions']
            },
            Chromium_CI: {
                base: 'Chromium',
                flags: ['--disable-gp','--disable-translate', '--disable-extensions', '--no-sandbox', '--remote-debugging-port=9223']
            },
            Firefox_1024x768: {
                base: 'Firefox',
                flags: ['-foreground', '-width', '1024', '-height', '768']
            },
            Firefox_headless: {
                base: 'Firefox',
                flags: ['-headless'],
                prefs: {
                    'network.proxy.type': 0
                }
            },
            LegacyEdge_1024x768: {
                base: 'BrowserStack',
                os: 'Windows',
                os_version: '10',
                browser: 'Edge',
                browser_version: '18.0',
                resolution: '1024x768',
                name: 'Unit tests on Legacy Edge'
            },
            Edge_1024x768: {
                base: 'BrowserStack',
                os: 'Windows',
                os_version: '10',
                browser: 'Edge',
                browser_version: '80.0',
                resolution: '1024x768',
                name: 'Unit tests on Chromium Edge'
            },
            Firefox_Win10: {
                base: 'BrowserStack',
                os: 'Windows',
                os_version: '10',
                browser: 'Firefox',
                browser_version: '53',
                resolution: '1024x768',
                name: 'Unit tests on Firefox Win10',
            },
            Chrome_OSX: {
                base: 'BrowserStack',
                os: 'OS X',
                os_version: 'Mojave',
                browser: 'Chrome',
                browser_version: '51',
                resolution: '1024x768',
                name: 'Unit tests on Chrome OSX',
            },
            Safari_OSX: {
                base: 'BrowserStack',
                os: 'OS X',
                os_version: 'Mojave',
                browser: 'Safari',
                browser_version: '12.1',
                resolution: '1024x768',
                name: 'Unit tests on Safari OSX',
            },
            Chrome_Win_latest: {
                base: 'BrowserStack',
                os: 'Windows',
                os_version: '10',
                browser: 'Chrome',
                browser_version: 'latest',
                resolution: '1024x768',
                name: 'Unit tests on Latest Chrome on Win 10'
            },
            Chrome_macOS_latest: {
                base: 'BrowserStack',
                os: 'OS X',
                os_version: 'Mojave',
                browser: 'Chrome',
                browser_version: 'latest',
                resolution: '1024x768',
                name: 'Unit tests on Latest Chrome on macOS Mojave'
            },
            Firefox_Win_latest: {
                base: 'BrowserStack',
                os: 'Windows',
                os_version: '10',
                browser: 'Firefox',
                browser_version: 'latest',
                resolution: '1024x768',
                name: 'Unit tests on Latest Firefox on Win 10',
            },
            Firefox_macOS_latest: {
                base: 'BrowserStack',
                os: 'OS X',
                os_version: 'Mojave',
                browser: 'Firefox',
                browser_version: 'latest',
                resolution: '1024x768',
                name: 'Unit tests on Latest Firefox on macOS Mojave',
            },
            Edge_Win_latest: {
                base: 'BrowserStack',
                os: 'Windows',
                os_version: '10',
                browser: 'Edge',
                browser_version: '80.0',
                resolution: '1024x768',
                name: 'Unit tests on Latest Chromium Edge on Win 10'
            },
            Safari_macOS_latest: {
                base: 'BrowserStack',
                os: 'OS X',
                os_version: 'Mojave',
                browser: 'Safari',
                browser_version: 'latest',
                resolution: '1024x768',
                name: 'Unit tests on Latest Safari on macOS Mojave',
            }
        },
        reporters,
        coverageIstanbulReporter: {
            reports: ['text-summary', 'lcov'],
            projectRoot: '../../',
            fixWebpackSourcePaths: true,
            combineBrowserReports: true,
            skipFilesWithNoCoverage: true,
            'report-config': {
                lcov: { projectRoot: '../../'}
            }
        },
        junitReporter: {
            outputDir: 'test-reports',
            suite: 'AUI'
        },
        preprocessors: {
            [testPattern]: 'webpack',
            [bootstrapPattern]: 'webpack',
        },
        webpack: webpackConfig,
        files: [
            require.resolve('jquery'),
            require.resolve('jquery-migrate'),

            {
                pattern: testPattern,
                watched: false,
            }
        ]
    });
};
