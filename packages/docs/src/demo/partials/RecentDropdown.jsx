const React = require('react');

const makeHref = require('../helpers/makeHref');

const MetalsmithContext = require('../../../build/MetalsmithContext');

module.exports = () => {
    const { menus, loggedIn, lang, translate } = React.useContext(MetalsmithContext);

    return (
        <>
            <a
                href="#"
                aria-controls="recent-dropdown"
                class="aui-dropdown2-trigger"
            >
                {translate('Recent')}
            </a>
            <aui-dropdown-menu id="recent-dropdown">
                <aui-section label={translate('Recent work')}>
                    {menus.recent.recentWork.map((page) => {
                        return (
                            <aui-item-link
                                key={page.id}
                                href={makeHref(page.id, loggedIn, lang)}
                            >
                                {page.label}
                            </aui-item-link>
                        );
                    })}
                </aui-section>
                <aui-section label={translate('Recently visited')}>
                    {menus.recent.recentlyVisited.map((page) => {
                        return (
                            <aui-item-link
                                key={page.id}
                                href={makeHref(page.id, loggedIn, lang)}
                            >
                                {page.label}
                            </aui-item-link>
                        );
                    })}
                </aui-section>
                <aui-section>
                    <aui-item-link href="#">{translate('Create new page')}</aui-item-link>
                </aui-section>
            </aui-dropdown-menu>
        </>
    );
};
