const ReactDOMServer = require("react-dom/server");
const React = require("react");

const Provider = require("./MetalsmithContext").Provider;
const makeRegisterPage = require("./utils/makeRegisterPage");
const labelToId = require("../src/demo/helpers/labelToId");
const makeHref = require("../src/demo/helpers/makeHref");

const auiVersion = "9.0.0";

module.exports = (files, metalsmith, done) => {
    // console.log(files);
    const jsxFilePaths = Object.keys(files).filter((key) =>
        key.endsWith(".jsx")
    );

    const i18n = require("../src/demo/fixtures/i18n");
    const languages = [
        {
            code: "en",
            content: require("../src/demo/fixtures/content-en"),
            homePageId: labelToId('Welcome'),
        },
        {
            code: "pl",
            content: require("../src/demo/fixtures/content-pl"),
            homePageId: labelToId('Witamy'),
        },
    ];

    jsxFilePaths.forEach((filePath) => {
        const filePathWithoutExtension = filePath.slice(0, -4);
        const fileName = filePathWithoutExtension.split("/").reverse()[0];

        // We assume that files started from uppercase are components
        // that we do not want to see as standalone html files in dist
        if (fileName[0] === fileName[0].toUpperCase()) {
            delete files[filePath];
            return;
        }

        // Those are valid only during the build time - we don't want them to be available
        // in dist directory
        if (
            fileName.includes("/demo/helpers/") ||
            fileName.includes("/demo/fixtures/")
        ) {
            delete files[filePath];
            return;
        }

        // We remove .jsx files from metalsmith and create .html files instead
        const referenceFile = files[filePath];
        delete files[filePath];

        languages.forEach((language) => {
            const lang = language.code;
            const prefix = 'demo/';
            const filePathWithoutPrefix = filePathWithoutExtension.slice(prefix.length);
            const newFileName = prefix + lang + '/' + filePathWithoutPrefix + ".html";

            files[newFileName] = JSON.parse(JSON.stringify(referenceFile));

            // We require given JSX component...
            const Component = require("../src/" + filePath);

            const translate = i18n(lang);

            const homePage = makeHref(language.homePageId, true, lang);

            // ...and use it to render static markup with no signs of React
            files[newFileName].contents =
                "<!doctype html>" +
                ReactDOMServer.renderToStaticMarkup(
                    <Provider value={{ ...metalsmith, auiVersion, translate, homePage, lang, i18n }}>
                        <Component />
                    </Provider>
                );
        });
    });

    languages.forEach((language) => {
        const lang = language.code;
        const content = language.content;
        const homePage = makeHref(language.homePageId, true, lang);

        const registerPage = makeRegisterPage({
            lang,
            content,
            i18n,
            files,
            metalsmith,
            auiVersion,
            homePage
        });

        content.pages.forEach((page) => {
            registerPage(page, true);
            registerPage(page, false);
        });
    });

    done();
};
