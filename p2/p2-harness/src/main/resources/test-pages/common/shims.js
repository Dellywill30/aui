/**
 * @fileOverview
 * The refapp includes Almond.js, then deletes the `window.define.amd` value.
 * That means we need to define a few things ourselves.
 */
/* global define */
if ('define' in window) {
    define('jquery', () => window.AJS.$);
    define('sinon', () => window.sinon);
}
