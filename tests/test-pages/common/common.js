/**
 * @fileOverview
 * These behaviours are meant to affect most test pages.
 */

require(['jquery'], function($) {

    function genericAlert(e) {
        const message = 'Dummy content clicked.';
        e.preventDefault();
        if (AJS.flag) {
            AJS.flag({
                body: message,
                close: 'auto',
            });
        } else {
            alert(message); // eslint-disable-line no-undef
        }
        console.debug(message, e.target);
        return false;
    }

    function disableTriggers() {
        const selectors = [];
        selectors.push('.auitest-disabletriggers a');
        selectors.push('.auitest-disabletriggers button');
        selectors.push('a[href="#"]');
        selectors.push('a[href*="example.com"]');
        $(document).on('click', selectors.join(', '), genericAlert);
    }

    // Initialise our side-effects
    disableTriggers();
});
