export let needsLayeringShim = sinon.stub();
export let supportsVoiceOver = sinon.stub();
export let supportsDateField = sinon.stub();
export let supportsFocusWithin = sinon.stub();

function reset() {
    needsLayeringShim.reset().returns(false);
    supportsVoiceOver.reset().returns(false);
    supportsDateField.reset().returns(true);
    supportsFocusWithin.reset().returns(false);
}

beforeEach(reset);
reset();
