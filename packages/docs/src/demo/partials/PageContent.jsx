const React = require('react');

const MetalsmithContext = require('../../../build/MetalsmithContext');

module.exports = () => {
    const { page } = React.useContext(MetalsmithContext);
    return <div dangerouslySetInnerHTML={{__html: page.content}}/>;
};
