SCRIPT_BIN_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

RELEASE_VERSION=$1
NEXT_VERSION=$2
BASEDIR=$3

# Check the appropriate parameters were provided to the script.
if [ -z "${RELEASE_VERSION}" -o -z "${NEXT_VERSION}" ]; then
  echo "Usage: $(basename ${0}) [release-version] [next-version] [<base dir>]"
  exit 1
fi


# Set and check the working directory
basedir=$([ -z $BASEDIR ] && echo $(pwd) || echo "$BASEDIR")
auidir=${basedir}

. "${SCRIPT_BIN_DIR}/requirements.sh" "$auidir"

##
# Bump AUI versions in preparation for building everything
#
cd ${auidir}
${auidir}/build/bin/bump-version.sh ${RELEASE_VERSION}
git commit -am "Release ${RELEASE_VERSION}."

