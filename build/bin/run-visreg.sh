#!/bin/sh
./build/bin/prepare-visreg.sh
# if preparation fails, it's because we didn't want to run visreg, so
# bail on the pipeline with a success
if [ $? -ne 0 ]; then
    echo "Skipping tests"
    exit 0
fi
yarn install --frozen-lockfile --non-interactive
yarn visreg/docs/ci
yarn visreg/flatapp/ci
./build/bin/update-visreg.sh
