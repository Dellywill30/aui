const React = require('react');
const MetalsmithContext = require('../../build/MetalsmithContext');
const AppHeader = require('./patterns/AppHeader');

const Document = require('./partials/Document');
const DemoBanner = require('./partials/DemoBanner');

module.exports = () => {
    const { translate, lang } = React.useContext(MetalsmithContext);

    return (
        <Document title="Home">
            <div id="page">
                <header id="header" role="banner">
                    <nav
                        class="aui-header aui-dropdown2-trigger-group"
                        aria-label="site"
                    >
                        <div class="aui-header-inner">
                            <div class="aui-header-primary">
                                <h1
                                    id="logo"
                                    class="aui-header-logo aui-header-logo-textonly"
                                >
                                    <a href="#">
                                        <span class="aui-header-logo-device">
                                            Kosmos
                                        </span>
                                    </a>
                                </h1>
                            </div>
                            <div class="aui-header-secondary">
                                <ul class="aui-nav">
                                    <li>
                                        <a
                                            href={`/demo/${lang}/pages/login.html`}
                                        >
                                            {translate('Log in')}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </header>

                <main id="main" role="main">
                    <div class="aui-page-panel">
                        <div class="aui-page-panel-inner">
                            <section class="aui-page-panel-content">
                                <h1>{translate('Kosmos intranet')}</h1>
                                <p>
                                    {translate(
                                        'You do not have access to this page.'
                                    )}
                                </p>
                            </section>
                        </div>
                    </div>
                </main>
            </div>
        </Document>
    );
};
