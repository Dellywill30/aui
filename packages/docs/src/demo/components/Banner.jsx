const React = require('react');

module.exports = ({ ariaLabel, children }) => {
    return (
        <section
            class="aui-banner aui-banner-error"
            aria-label={ariaLabel}
        >
            {children}
        </section>
    );
};
