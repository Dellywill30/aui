import $ from '@atlassian/aui/src/js/aui/jquery';

/* global mocha, chai, sinon, __webpack_require__ */

window.expect = chai.expect;

// Chai extensions
// ---------------
chai.use(function (chai, utils) {
    utils.addProperty(chai.Assertion.prototype, 'visible', function () {
        var $el = $(this._obj);
        this.assert(
            $el.is(':visible') === true,
            'expected "#{this}" to be visible',
            'expected "#{this}" to be hidden'
        );
    });
});
