# 9.3.0
* [Documentation](https://aui.atlassian.com/aui/9.3/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.3.0)

## Changed
* Swap jQuery.tipsy with Popper.js to handle AUI tooltips (AUI-2760)

# 9.2.0
* [Documentation](https://aui.atlassian.com/aui/9.2/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.2.0)

## Added
* Sortable Table now accepts options via its JavaScript API. See the documentation for details.
* Badges have three new variants: `.aui-badge-added`, `.aui-badge-removed`, and `.aui-badge-important`.

## Changed
* Bumped build and minifier dependencies.
* The navigation items CSS pattern now accepts arbitrary elements as children, not just `<span>` or `<a>` elements.

## Fixed
* Date picker's background colour is correct in light mode.
* Select2's background color is set correctly.
* Chevrons do not repeat on standard `<select>` elements.

## Removed
* The `Navigation#hideMoreItems` method has been removed -- it has not worked for years and nobody seemed to mind ;)

# 9.1.6
* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.6)

## Fixed
* Aligned underline for link button that uses text and icon (AUI-5306)

# 9.1.5
* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.5)

## Fixes
* Tabbing order in AUI dialog2 component now takes into account iframes (AUI-5290)

# 9.1.4
* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.4)

## Fixed
* Missing accessibility features in sidebar.soy template (AUI-5244)
* Application header styles for logo match other header items (AUI-5276)

# 9.1.3
* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.3)

## Fixed
* Fixed XSS vulnerability in Dropdown2.

# 9.1.2
* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.2)

## Fixed
* Form fields elements do not render double-sized focus-ring in browsers that natively support :focus-visible
* Expander component behaves properly when trying to toggle multiple times

## Changed
### Documentation
* AUI Documentation now properly describes multiple ways to consume AUI

### Single Select
* aui-option uses encodeURI for img-src escaping, allowing for usage of URLs with additional parameters
  and/or special characters

# 9.1.1
* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.1)

## Fixes
* Trigger component no longer causes DOM exception when there are cross-origin iframes in the document

# 9.1.0
* [Documentation](https://aui.atlassian.com/aui/9.1/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.1.0)

## Fixed
### Dropdown
* Dropdown aligns properly when rendered in legacy Edge.

## Changed
* AUI's version of `Popper.js` has been bumped to v2.4.4, up from v1.14.5.
* layered components expose data-popper-placement attribute instead of x-placement as their internal API.
   x-placement attribute has been DEPRECATED and is kept for backwards compatibility only.

# 9.0.3
* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.3)

## Fixed
### Documentation
* Contrast on elements in FlatApp has been corrected

### Select component
* Chevrons in select fields were adjusted for dark mode

### Layered components
* All layered components (i.e.: Dropdown) position correctly if loaded inside iFrame.

# 9.0.2
* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.2)

## Changed
### Date Picker component
* AUI date picker widgets can be destroyed and re-created

## Fixed
### Dropdown component
* Buttons in Dropdown items span the Dropdown's width properly

# 9.0.1
* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.1)

## Changed
### Toggle button component
* Documentation now correctly states that label is accepted as attribute only.
* It used to state that the component may accept label as attribute and property. It was never true.

### Documentation
* Dark mode toggle is now available across all our demo and test apps.

## Fixed
### Flag component
* Flag uses legacy-browsers friendly way of looping trough NodeList

### Toggle button component
* Toggle buttons should retain their icon size in more places.

# 9.0.0
* [Documentation](https://aui.atlassian.com/aui/9.0/)
* [JIRA issues](https://ecosystem.atlassian.net/issues/?jql=project=AUI%20and%20fixVersion=9.0.0)

## Highlights
* AUI no longer supports IE 11.
* AUI now has dark mode support!
* AUI's layered components have been audited and updated to improve their accessibility.
* AUI's patterns now include skip-links and landmarks for better screen reader and keyboard navigation support - they are applayed on docs website as well.
* All patterns planned to be removed in 9.0.0 will be removed in 10.0.0 instead.

## Added
* Dark mode for all AUI components and patterns. It can be enabled by adding an `aui-theme-dark` CSS class to `<body>`.
* A new skip link pattern has been added and documented.
* A new `.aui-close-button` pattern has been added. All elements that can be closed now use this pattern.
* All focusable elements have an explicit `:focus` and `:focus-visible` style by default.
* Form fields auto-filled by the browser have a new custom style.
* All layered elements may specify a dom container that will be used as appending point upon render.
    * Read the AUI 9 upgrade documentation for details on how to make best use of this property.

## Changed
* AUI's version of `Underscore` has been bumped to v1.10.2, up from v1.9.2.
* All AUI components should meet WCAG 2.0 AA colour contrast rates between text and background colours when used with AUI's page layout patterns.
* Layered components are considered closed by default. They are visible once an `open` attribute is added to them.
* Layered components reset their font-size to `1rem` and `text-align` to `left`.
* Layered components, except Dialog2, are no longer appended to the body.
* Animations for layered components are based on the presence or absence of an `open` attribute. Changing the `hidden` attribute is immediate and does not animate.
* Callbacks registered using a Dialog2 instance's `on` and `off` methods will only trigger when that specific dialog fires the event, as opposed to nested components.
* The `.aui-page-header`, `.aui-group`, `.aui-buttons`, and other positional patterns now use CSS flexbox for layout.
* AUI's datepicker no longer displays the placeholder by default. The config API was extended to set the placeholder explicitly.

### Avatar component
* Avatar sizes are now controlled by the `--aui-avatar-size` CSS property.

### Button component
* Button colours are now controlled by the `--aui-btn-bg`, `--aui-btn-border`, and `--aui-btn-text` CSS properties.
    * Read the AUI button documentation for details on how to make best use of these properties.

### Dropdown component
* ARIA attributes are no longer given discrete styles. CSS classes or basic HTML elements should be used instead.
    * Replaced selection of `[role=menuitem]` with `a` and `button`.
    * Replaced selection of `[role=menuitemcheckbox]` and `[role=checkbox]` with `.aui-dropdown2-checkbox`.
    * Replaced selection of `[role=menuitemradio]` and `[role=radio]` with `.aui-dropdown2-radio`.

### Icon component
* Icons are now `display:flex`. Icon glyphs are centered within the icon's display box using `margin:auto`.
* Icon sizes are now set by the `--aui-icon-size` CSS property, initially set within `.aui-icon`.
* Icon glyphs are now set by the `--aui-ig` CSS property, initially set within `.aui-icon`.

### Message component
* Message dimensions are now set by `--aui-message-padding` and `--aui-message-gutter`, initially set on `.aui-message`.
* Message dimensions are now re-used and selectively overridden by the Flag and Banner patterns.

### Page CSS reset
* Base font-size is defined on `html` instead of `body`. This means `rem` units will reflect AUI's default font size.
* CSS normalization rules needed for IE9, IE10 and IE11 have been removed.
* The hidden attribute is now declared with `display: none !important `.

### Page layout
* Page layout has been updated to simplify the placement of sidebar, <main>, and other content regions.
* Page layout and application header patterns now document skip links that should be added to a page, such that keyboard-only users can quickly jump to specific page regions and important actions.
* If the page includes a sidebar, the `#content` element should have only 2 child elements:
    * `<section class="aui-sidebar" aria-label="sidebar">` - houses the sidebar and its content.
    * `<section aria-label="page">` - houses all other page content.
    * See the updated page layout documentation, or the upgrade guide, for more details.

### Sidebar
* We changed `a` element from extend button to actual `button` element.
* We now use iconfont `aui-iconfont-chevron-double-left` instead of custom icon for the extend button.
* Sidebar width is now set by the `--aui-sidebar-width` CSS property, initially set within `.aui-page-sidebar`.

## Fixed
* Any dropdown trigger element that references a non-existent element will no longer throw an error on click or hover.
* Any dropdown item element with the `aui-dropdown2-interactive` class will *always* prevent default for any event triggered on it.
    * This is in line with the intent and behaviour described in the documentation.
    * Previously, events would be passed through if the item was either checked or disabled.
* Opening a dropdown via the down arrow no longer causes the document scroll position to jump.
* Layer positioning accounts for triggers inside iframe containers and should position themselves correctly.
* Date picker's year suffix will no longer render as the literal string "null" by default.
* Layered element's shadows should be visible in Edge.

### Accessibility
* All invalid usage of `aria-hidden` has been removed.
    * Read the upgrade guide to determine whether you need to make similar changes to your markup or usages of AUI components.
* All close buttons in all AUI components are part of the tab order and have a consistent label of "Close".
* Modal dialog accessibility is improved:
    * Modal dialogs are given keyboard focus and announced to screen readers when opened.
    * The documentation now outlines how to give a modal dialog an appropriate accessible label.
* Inline dialog accessibility is improved for toggle behaviour:
    * The contents of an open inline dialog can now be navigated to using only the keyboard.
    * Inline dialog element is now focused after opening.
    * The documentation now outlines how to give an inline dialog an appropriate accessible label.
  Note that the hover behaviour variant of inline dialog is inaccessible and is now deprecated.
* Application header menu accessibility is improved:
    * The pattern's `<nav>` element must now be labelled as "site".
    * The site logo is now a `<span>` instead of an `<h1>`. The `<h1>` is reserved for the page's main content heading.
* Dropdown component accessibility is improved:
    * Incorrect use of `role=application` and `role=presentation` have been removed.
    * The styles for hover and focus are visually distinct, making it easier for people using a keyboard and mouse simultaneously to tell what item has keyboard vs. mouse focus.
    * The first focusable item of a dropdown is focussed on opening the dropdown. This eases access for NVDA and JAWS users.
    * Dropdown group headings will be announced as focus moves in to the group. The headings are no longer navigable by screen reader's virtual cursors.
* Message pattern accessibility is improved:
    * Link and button text colours are now set to the message's standard text colour and are given an underline, to ensure colour contrast ratios are met.
    * Focus indicators are set to the message text colour, to ensure contrast ratios are met.

## Deprecated
* Inline dialog 2 hover functionality is deprecated and will be removed in the future.
* populateParameters function and params object is deprecated and will be removed in AUI 10.0.0.
* AUI's legacy dropdown1 component has been extracted to its own `@atlassian/aui-legacy-dropdown` package.
* AUI's legacy toolbar1 component has been extracted to its own `@atlassian/aui-legacy-toolbar` package.

## Removed
* All Atlassian brand logos have been removed. You can access them from the [@atlassian/brand-logos](https://www.npmjs.com/package/@atlassian/brand-logos) package.
* The `.aui-legacy-focus` class support has been removed. All focusable elements should have an appropriate focus ring applied.
* Message icons - we removed following classes
    * .aui-message .icon-close
    * .aui-icon-close
    * .aui-message .icon-close-inverted,
    * .aui-message.error .icon-close,
    * .aui-icon-close-inverted
  The `.aui-close-button` pattern should be used instead.
* jQuery ajaxSettings configuration was removed. Please specify it globaly for product or add it per request where needed.

# Previous versions

- [8.x.x](./changelogs/8.x.x.md)
- [7.x.x](./changelogs/7.x.x.md)
- [6.x.x](./changelogs/6.x.x.md)
- [5.x.x](./changelogs/5.x.x.md)
- [4.x.x](./changelogs/4.x.x.md)
