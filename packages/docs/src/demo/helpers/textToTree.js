/* eslint-disable */

const labelToId = require('./labelToId');

module.exports = (text) => {
    const result = text.split('\n').reduce(
        (result, next) => {
            const { lastItem, roots } = result;
            const indent = next.split('').findIndex((char) => char !== ' ') / 4;

            // console.log(next, indent);

            const label = next.trim();
            const id = labelToId(label);

            if (indent < 0) {
                // for empty lines
                return result;
            }

            if (lastItem === null || indent === lastItem.indent) {
                if (lastItem === null || lastItem.parent === null) {
                    result.lastItem = {
                        parent: null,
                        indent,
                        label,
                        id,
                        children: [],
                    };
                    roots.push(result.lastItem);
                    return result;
                }

                if (lastItem.parent !== null) {
                    result.lastItem = {
                        parent: lastItem.parent,
                        indent,
                        label,
                        id,
                        children: [],
                    };
                    lastItem.parent.children.push(result.lastItem);
                    return result;
                }
            }

            if (lastItem.indent - indent === 1) {
                let parent = lastItem.parent.parent;
                result.lastItem = {
                    parent,
                    indent,
                    label,
                    id,
                    children: [],
                };
                if (parent === null) {
                    roots.push(result.lastItem);
                } else {
                    parent.children.push(result.lastItem);
                }
                return result;
            }

            if (lastItem.indent - indent > 1) {
                let parent = lastItem.parent;
                while (parent.indent !== indent) {
                    parent = parent.parent;
                }
                parent = parent.parent;
                result.lastItem = {
                    parent,
                    indent,
                    label,
                    id,
                    children: [],
                };
                if (parent === null) {
                    roots.push(result.lastItem);
                } else {
                    parent.children.push(result.lastItem);
                }
                return result;
            }

            if (indent > lastItem.indent) {
                result.lastItem = {
                    parent: lastItem,
                    indent,
                    label,
                    id,
                    children: [],
                };
                lastItem.children.push(result.lastItem);
                return result;
            }

            return result;
        },
        { lastItem: null, roots: [] }
    );

    // function replacer(key, value) {
    //     // Filtering out recursive properties
    //     if (key === "parent") {
    //         return undefined;
    //     }
    //     return value;
    // }

    // console.log(JSON.stringify(result.roots, replacer, 2));

    return result.roots;
};
