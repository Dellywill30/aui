const React = require('react');

const makeHref = require('../helpers/makeHref');

const MetalsmithContext = require('../../../build/MetalsmithContext');


module.exports = () => {
    const { menus, loggedIn, lang, translate } = React.useContext(MetalsmithContext);

    const links = menus.spaces
        .map(({ id, label }) => {
            return [
                makeHref(id, loggedIn, lang),
                label,
            ];
        })
        .map(([href, label], index) => {
            return (
                <aui-item-link href={href} key={index}>
                    {label}
                </aui-item-link>
            );
        });

    return (
        <>
            <a
                href="#"
                aria-controls="spaces-dropdown"
                class="aui-dropdown2-trigger"
            >
                {translate('Spaces')}
            </a>
            <aui-dropdown-menu id="spaces-dropdown">
                <aui-section label={translate('Recent spaces')}>{links}</aui-section>
                <aui-section>
                    <aui-item-link href="#">{translate('Space directory')}</aui-item-link>
                    <aui-item-link href="#">{translate('Create space')}</aui-item-link>
                </aui-section>
            </aui-dropdown-menu>
        </>
    );
};
